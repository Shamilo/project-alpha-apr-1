from django.urls import path
from projects.views import list_projects, make_project
from projects.views import show_task


urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", show_task, name="show_project"),
    path("create/", make_project, name="create_project"),
]
